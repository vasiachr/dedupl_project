Advertisement deduplication
===========================

Getting started steps
---------------------

First you will need to create and activate a virtualenv and install the dependencies:

~~~
$ virtualenv --python=python3.5 env
$ . env/bin/activate
$ pip install -r requirements.txt
~~~

Then you will need to create a data directory and move the data (images, data.txt) inside it.

You will need to download and extract within the data directory the pre-trained GloVe vectors
https://nlp.stanford.edu/projects/glove/

The data directory should have the following structure:

* images/
* data.txt
* glove.6B.50d.txt


Generating dataset
------------------

You can create the dataset dump file with the following command:

~~~
$ python dataset.py
~~~

ML experimentation
------------------

Please run the jupyter notebook (experiment.ipynb) to view the training/evaluation process.

Backlog
-------

* Unit tests
* Command line interfacing
