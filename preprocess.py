import config

from loaders import load_text_data, load_image_data, load_embeddings_model
from tqdm import tqdm
from utils import text_vector, image_vector


def text_vectors(data, model):
    """
    Generates embedding vectors for text
    :param data: list of ad text dictionaries
    :param model: embeddings_model
    :return: dictionary with cc as key and embeddings as value
    """
    return {
        d['cc']: text_vector(d['title'], model) for d in tqdm(data)
    }


def image_vectors(data):
    """
    Generates image feature vector
    :param data: list of image ad dictionaries
    :return: dictionary with cc as key and image feature vector as value
    """
    return {
        d['cc']: image_vector(d['path'], vector_size=16) for d in tqdm(data)
    }


def run():
    """
    Generates text and image vectors
    :return: text vectors, image vectors
    """
    text_data = load_text_data(config.TEXT_DATA_FILE)
    print(len(text_data))
    image_data = load_image_data(config.IMAGES_PATH)
    print(len(image_data))
    model = load_embeddings_model(config.GLOVE_INPUT_FILE, config.WORD2VEC_OUTPUT_FILE)
    print(len(model.vocab))

    text_vec = text_vectors(text_data, model)
    img_vec = image_vectors(image_data)

    return text_vec, img_vec


if __name__ == "__main__":
    run()
