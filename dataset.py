import numpy as np

from joblib import load, dump
from labels import run as get_labels
from tqdm import tqdm
from preprocess import run as get_vectors


def run():
    text_vectors, img_vectors = get_vectors()
    labels = get_labels()

    dataset = []

    for pair in tqdm(labels):
        try:
            first_text = text_vectors[str(pair['first'])]
            second_text = text_vectors[str(pair['second'])]
            first_img = img_vectors[str(pair['first'])]
            second_img = img_vectors[str(pair['second'])]
            label = [pair['label']]
            if all([v is not None for v in [first_text, second_text, first_img, second_img]]):
                dataset.append(np.concatenate(
                    [first_text, second_text, first_img, second_img, label]
                ))
        except:
            continue

    dataset = np.array(dataset)

    with open('dataset.pkl', 'wb') as f:
        dump(dataset, f)


if __name__ == "__main__":
    run()
