import config

from collections import defaultdict
from itertools import combinations
from random import choice, shuffle
from loaders import load_text_data


class Ad:

    def __init__(self, cc, label, *args, **kwargs):
        self.cc_ = cc
        self.label_ = label

    @property
    def cc(self):
        return int(self.cc_)

    @property
    def label(self):
        return int(self.label_)

    def __bool__(self):
        return self.cc_.isdigit() and self.label_.isdigit()


def construct_pair_dataset(data):
    """
    Constructs a pairwise dataset of cc and labels. Pair of ads that belong in the same cluster belong to class 1 and 0
    otherwise.
    :param data: list of ad text dictionaries
    :return: list of dictionaries with first, second and label as keys and the equivalent ccs and true label as values
    """

    data = [Ad(cc=d['cc'], label=d['label']) for d in data]
    data = [d for d in data if d]
    clusters = defaultdict(list)
    dataset = []

    for d in data:
        clusters[d.label].append(d)

    for c, dat in clusters.items():
        for combo in combinations(dat, 2):
            dataset.append({'first': combo[0].cc, 'second': combo[1].cc, 'label': 1})

    print('Constructed pairs for class 1: {}'.format(len(dataset)))

    for d in data:
        counter = 0
        while counter < 8:
            neg = choice(data)
            if d.label != neg.label:
                dataset.append({'first': d.cc, 'second': neg.cc, 'label': 0})
                counter += 1

    print('Total pairs: {}'.format(len(dataset)))

    shuffle(dataset)
    return dataset


def run():
    data = load_text_data(config.TEXT_DATA_FILE)
    return construct_pair_dataset(data)


if __name__ == "__main__":
    run()
