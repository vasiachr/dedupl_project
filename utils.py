import cv2
import numpy as np

from nltk import word_tokenize
from scipy.misc import imread

english_stopwords = {"i", "me", "my", "myself", "we", "our", "ours", "ourselves", "you", "your", "yours", "yourself",
                     "yourselves", "he", "him", "his", "himself", "she", "her", "hers", "herself", "it", "its",
                     "itself", "they", "them", "their", "theirs", "themselves", "what", "which", "who", "whom", "this",
                     "that", "these", "those", "am", "is", "are", "was", "were", "be", "been", "being", "have", "has",
                     "had", "having", "do", "does", "did", "doing", "a", "an", "the", "and", "but", "if", "or",
                     "because", "as", "until", "while", "of", "at", "by", "for", "with", "about", "against", "between",
                     "into", "through", "during", "before", "after", "above", "below", "to", "from", "up", "down", "in",
                     "out", "on", "off", "over", "under", "again", "further", "then", "once", "here", "there", "when",
                     "where", "why", "how", "all", "any", "both", "each", "few", "more", "most", "other", "some",
                     "such", "no", "nor", "not", "only", "own", "same", "so", "than", "too", "very", "s", "t", "can",
                     "will", "just", "don", "should", "now"}


def text_vector(title, embeddings_model):
    """
    Generates the text vector embedding for each ad title
    :param title: title of ad
    :param embeddings_model: the pre-trained embeddings_model
    :return: array vector of text embedding
    """
    sentence = [w.lower() for w in word_tokenize(title) if w.lower() not in english_stopwords]
    return np.mean([
        embeddings_model[word]
        for word in sentence
        if word in embeddings_model.vocab], axis=0)


def image_vector(path, vector_size=16):
    """
    Generates the image feature vector by detecting the keypoints of image(center points of local patterns) using
    the KAZE algorithm , build the vector descriptors based on the keypoints.
    :param path: image file
    :param vector_size: 
    :return:
    """
    img = imread(path, mode='RGB')
    try:

        kz = cv2.KAZE_create()
        kps = sorted(kz.detect(img), key=lambda x: -x.response)[:vector_size]

        kps, feat_vect = kz.compute(img, kps)

        feat_vect = feat_vect.flatten()
        needed_size = (vector_size * 64)
        if feat_vect.size < needed_size:
            feat_vect = np.concatenate([feat_vect, np.zeros(needed_size - feat_vect.size)])

        return feat_vect

    except cv2.error:
        return None

    except AttributeError:
        return None
