import csv
import os

from gensim.scripts.glove2word2vec import glove2word2vec
from gensim.models import KeyedVectors


def load_text_data(data_file):
    """
    Load text of ad to list of dictionaries
    :param data_file: directory of data.txt
    :return: list
    """
    return list(csv.DictReader(open(data_file), delimiter=';', quoting=csv.QUOTE_NONE))


def load_image_data(data_path):
    """
    Load images data to list of dictionaries
    :param data_path: directory of images file
    :return: list of dictionary with cc, label, path as keys and their equivalent values as values
    """
    img_files = os.listdir(data_path)
    return [{'cc': img.split('_')[1], 'label': img.split('_')[0], 'path': data_path + img} for img in img_files]


def load_embeddings_model(glove_input_file, word2vec_output_file):
    """
    Generates the pre-trained embeddings model using the gensim API.
    :param glove_input_file: directory of the glove embeddings txt file
    :param word2vec_output_file: directory of the transformed word2vec_glove file
    :return: embeddings_model
    """
    glove2word2vec(glove_input_file, word2vec_output_file)
    return KeyedVectors.load_word2vec_format(word2vec_output_file, binary=False)
